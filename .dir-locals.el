;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((org-mode . ((org-html-postamble . nil)
              (org-export-with-section-numbers . nil)
              (org-export-with-toc . nil)
              (org-html-self-link-headlines . nil)
              (eval . (require 'ox-tufte)))))
