#+TITLE: CLOSOS: a Lisp operating system
#+OPTIONS: toc:nil author:nil

CLOSOS [fn:1] is a project to create an operating system written in and inspired by the [[https://lisp-lang.org/][Common Lisp]] programming environment.

[fn:1] Variously pronounced "close oh ess", "close ohs", or even "colossus".


It aims to provide -
- a single address space (instead of processes),
- object-capability security, an object store (instead of a hierarchical file system),
- a single memory abstraction (instead of primary and secondary memory),
- multiple global environments, and safe concurrency.

It will start out as a single process on existing operating systems, rather than aiming to run on bare metal from the get-go.

To learn about its design in more detail, see the paper [[https://metamodular.com/closos.pdf][CLOSOS: Specification of a Lisp operating system]].

# TODO
# - the benefits of the characteristics
# - the people behind the project
# - the applications it can run

* Status
As of January 2023, the CLOSOS community is writing [[https://codeberg.org/CLOSOS/projects/issues?q=&type=all&sort=&state=open&labels=65713&milestone=0&assignee=0&poster=0][applications for CLOSOS]] and working on [[https://github.com/robert-strandh/sicl][SICL]], a new implementation of Common Lisp which supports first-class global environments.

* Join us
If this sounds interesting and you wish to help, get in touch with us on libera.chat, in [[irc://irc.libera.chat/#commonlisp][#commonlisp]], [[irc://irc.libera.chat/#clim][#clim]], or [[irc://irc.libera.chat/#sicl][#sicl]], and see if you would like to contribute to the [[https://codeberg.org/CLOSOS/projects][suggested projects]].
